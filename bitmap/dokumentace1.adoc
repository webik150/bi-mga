= Obsah

* link:#user-content-fotka-1[Fotka 1]
* link:#user-content-fotka-2[Fotka 2]

= Fotka 1

== Zdrojové soubory

* link:bitmap/Uloha1Obr1.psd[Source PSD]
* link:bitmap/src1/2019-10-10_11.28.01.png[Nether portal]  (SRC: Fotil jsem)
* link:bitmap/src1/_DSC0030.NEF[Dopravní prostředek] (SRC: Fotil jsem)
* link:bitmap/src1/_DSC0038.NEF[Prostředí] (SRC: Fotil jsem)
* link:bitmap/src1/_DSC0071.NEF[Já] (SRC: Fotila sestra)


== Postup

* Naimportuji fotky do photoshopu a upravím světlo tak, aby bylo na všech fotkách podobné. Většinou stačí upravit teplotu nasvícení. U fotky popelnice musím také snížit jas světel (highlights).

image::https://i.imgur.com/D3IC7kB.png[width=600]

* Fotky rovnou rozmístím na jejich finální pozice (zvýším jejich průhlednost, aby se lépe umisťovaly) a podle potřeby je zvětším/zmenším.

image::https://i.imgur.com/CS5MVRU.png[width=600]

* Zjišťuji, že fotka popelnice je nepoužitelná kvůli perspektivě, a tak opakuji kroky 1 a 2 s jinou fotkou.
* U fotek mě a popelnice vytvořím masku a štětcem s velkým průměrem nejdříve nahrubo zamaskuji nechtěné části. Poté začnu s jemnějším štětcem odstraňovat nedostatky. Nejtěžší jsou vlasy, ale snažím se držet rad, co nám dal Vojtěch Tomas na cvičení, konkrétně neoříznout vlasy moc "rovně" a nechat okraje lehce průsvitné. Pro finální úpravy jsem použil štětce z kolekce Natural Brushes, aby nebyly okraje tak pravidelné.
* Při ořezávání své ruky jsem zjistil, že do obrázku úplně nesedí, a tak jsem ji vyměnil za pirátský hák.
* Jelikož jsem exteriér fotil asi 10 minut předtím, než začalo pršet, nejsou na fotce žádné stíny, kromě tzv. ambient occlusion (https://www.youtube.com/watch?v=AtcI8MY9-sw), tzn. stíny generované zastíněním ambientního světla z okolí. Ty jsem se snažil napodobit pomocí Burn Tool. Konkrétně pod mojí ponožkou, pod popelnicí, a na rozhraních mezi kameny a popelnicí.

image::https://i.imgur.com/MThxptj.png[]

* Jako první vliv na okolí jsem popelnici "zasadil" do trávy, čehož jsem docílil narazítkováním okolní trávy do nové vrstvy pomocí Stamp Tool s 40% průhledností.

image::https://i.imgur.com/yueR1Ve.png[width=600]

* Po inspekci obrázku z větší dálky  upravuji kontrast a popelnice a lehce ji ztmavuji (pomocí Brightness and Contrast) (Image/Adjustments/Brightness and Contrast).
* Došlo mi, že nemám práva na obrázek háku, který jsem použil, a tak jsem si musel odmaskovat ruku. Upravil jsem ji pomocí Burn Tool a snížením kontrastu.
* Přidal jsem portál z Minecraftu. Obrázek jsem pořídil ve hře. Ve fotomontáži jsou 2 kopie. Jedna pro část portálu nad popelnicí (s opacitou 50%) a jedna pro část pod popelnicí (s opacitou 29%). Obě části jsou zamaskované měkkým štětcem tak, aby pasovaly do kameného portálu. Na prostředním kameni jsem nechal obrázek trochu přečnívat, abych simuloval záři (Jak je vidět na následujícím obrázku, netrefil jsem správně perspektivu.).

image::https://i.imgur.com/LJ3Ca9g.png[]

* Jedná se o téměř neviditelný detail, ovšem v popelnici je můj odraz. Ten jsem přidal tak, že jsem zduplikoval layer s mojí postavou, ořízl jej podle masky, zezrcadlil jej, rozmazal gaussovým rozostřením, nastavil popelnici jako clipping masku (tzn zobrazí se jenom pixely na popelnici a ne okolo), a nakonec nastavil Opacity vrstvy na 4%.
* Vzpomněl jsem si, že musíme do obrázku přidat název. Razítkem jsem vymazal původní nápis na jmenovce sochy. Poté jsem přidal text "Nové metro" a pomocí Perspective tool jsem jej naaranžoval tak, aby na jmenovku pasoval. Poté jsem změnil blendmode vrstvy na Vivid Light a barvu textu na světle šedivou. Tím dostal text metalický nádech a zároveň se hezky spojil s původním odleskem na desce. Nakonec jsem textu v Blending Options přidal vnitřní stín a Bevel (Musel jsem si pohrát se směrem obou efektů tak, aby správně fungovaly s perspektivou fotky).

image::https://i.imgur.com/qwn9AS9.jpg[width=600]

* Zjistil jsem, že musíme mít 2 vlivy na okolí, a tak jsem ještě narychlo přidal do trávy koleje od kol popelnice.
** Stačilo narazítkovat kus hlíny z dolního levého rohu obrázku s nízkým krytím a poté lehce zakrýt okolní trávou.
** Narazil jsem na problém, protože jsem smíchal moc ruzných částí trávy do sebe a výsledek vypadal velmi rozmazaně (opravit to šlo přidáním razítku tvrdosti).
** Pokusil jsem se i přidat kousky hlíny na kameny mezi portálem a sochou (razítkem s nepravidelným tvarem), ovšem jsou velmi jemné.

image::https://i.imgur.com/fR2FRsZ.png[width=600]

image::https://i.imgur.com/lx7Blmq.png[]

 Zatial: 1.5h focení + 4.5h práce

== Progres

image::https://i.imgur.com/uCXRaif.jpg[width=600]

image::https://i.imgur.com/7Ej1YIp.jpg[width=600]

image::https://i.imgur.com/5Wdm3yr.jpg[width=600]

image::bitmap/Uloha1Obr1.jpg[width=600]

image::bitmap/src1/Uloha1Progress2.jpg[width=600]

= Fotka 2

== Zdrojové soubory

* link:bitmap/Uloha1Obr2.psd[Source PSD]
* link:bitmap/src2/tesla_motors_supercharger_18.jpg[Nabíječka] (link:http://samochodyelektryczne.org/img/tesla_motors_supercharger/zoom/tesla_motors_supercharger_18.jpg[SRC])
* link:bitmap/src2/_DSC0022.NEF[Dopravní prostředky] (SRC: Fotil jsem)
* link:bitmap/src2/_DSC0036.NEF[Prostředí] (SRC: Fotil jsem)
* link:bitmap/src2/_DSC0051.NEF[Já] (SRC: Fotila sestra)
* link:bitmap/src2/s-l1000.jpg[Indikátor stavu baterie] (link:http://i.ebayimg.com/images/i/361086965264-0-1/s-l1000.jpg[SRC])

== Postup

* Stejně jako u první fotky jsem začal umístěním obrázků sebe a popelnic.
* U fotky popelnic upravím barvy pomocí Color Balance (na hodnoty -3, +8, +10) (Image/Adjustments/Color Balance).
** Tím popelnice lehce zmodraly a sladily se s pozadím.

* Zahlazení odlesků z původního nadchodu:

image::https://i.imgur.com/Td9KaJf.png[width=600]

* Vymazání dopravní značky.
** Použil jsem stamp tool.
** Nejdříve jsem ceduli zamazal "nahrubo". Razítkem s malým poloměrem a velkou tvrdostí.
** Poté jsem měkkčím razítkem s velkým poloměrem a 50% opacitou nanesl texturu z desek na každé straně, aby se zprůměrovala světlost a nebyly vidět tahy.

NOTE: Nakonec mi došlo, že musím přidat nápis a musím menší panely sjednotit do jednoho. To je bohužel až ve výsledné fotce.

image::https://i.imgur.com/9RAMv0r.png[width=600]

* Pomocí štětce v módu Multiply jsem ztmavil některé odlesky.

image::https://i.imgur.com/8ClnprL.png[]

* Popelnice jsem ztmavil pomocí Levels (Image/Adjustments/Levels).
** Tady jsem měl na výběr nespočet možností, jak popelnice ztmavit. Levels jsem vybral vpodstatě jen abych pořád nepoužíval Contrast & Brightness a nebo Hue, Saturation, Brightness.

image::https://i.imgur.com/WJau6wd.png[]

* Abych simuloval sklo, vložil jsem na vrch novou vrstvu s upravenou kopií nejspodnější vrstvy. Vypadá naprosto ohavně, ale s maskou nastavenou na 19% vytváří hezké neinvazivní odlesky.
** K úpravě této vrstvy jsem použil nejdříve razítko, kterým jsem se snažil vymazat zadní rámy okna, a poté kopírování obdélníků již vyčištěného okna tak, abych zakryl celou plochu.  

image::https://i.imgur.com/Ea6ky4m.png[width=600]

* Pomocí štětce v módu Multiply jsem se ztmavil.

image::https://i.imgur.com/WyCKne9.png[width=600]

* Přidání nápisů mi trvalo asi nejdéle a stejně s nimi nakonec nejsem úplně spokojený. (Nabíjecí stanice VODPADu) (VOyage-Dandying Plants-Adoring Drivable)
** Nejdříve jsem musel sjednotit panely nadchodu do jednoho, aby se tam nápis vešel.
** Myslím, že nejlepší by bylo vymodelovat 3D text a ten pak do fotky dofotomontážovat, ale na to jsem neměl čas.
** Zjistil jsem, že se stínem vypadá text ve scéně přirozenějí, ovšem ve scéně žádné stíny nejsou, a tak jsem stín zase smazal.
** Zkoušel jsem si hrát s blend mody, opacitou, fonty, barvami, efekty typu bewel a emboss, ovšem nepoařilo se mi docílit toho, aby text vypadal přirozeně.
** Nakonec jsem pouze v Blending Options zapnul Pillow Emboss a snížil viditelnost jak stínů tak světel, a poté vytvořil na pravé straně stín pomocí štětce v módu Darken.

image::https://i.imgur.com/UoUSsGS.png[width=600]

image::https://i.imgur.com/azoyn0Q.png[width=600]

* Přidání samotné nabíjecí stanice.
** Musel jsem celkem hodně upravit barvy a jas. Použil jsem Brightness and Contrast, Hue/Saturation i Color Balance, abych se co nejvíce přiblížil barvám okolí.

image::https://i.imgur.com/CC7HSy6.png[]

image::https://i.imgur.com/83i3JSi.png[]

* Přidání indikátoru stavu baterie
** Nejdříve jsem přidal šum pomocí Filter/Noise/Add Noise.
** Poté jsem upravil perspektivu tak, aby indikátor pasoval na víko popelnice.
** Poté jsem vyřízl červené světýlko indikátoru, vložil jej do nové vrstvy a v Blending Options jsem zapnul Outer Glow s červenou barvou.
** Nakonec jsem měkkou gumou zamazal okraje.

image::https://i.imgur.com/mL5ci2G.png[width=600]

image::https://i.imgur.com/6SBC3LQ.png[]

* Přepsání TESLA na VODPAD (toto není útok na Teslu, jedná se o název futuristické popelnice a Tesla převládá na Google Images, když člověk hledá nabíječku na auto).
** Pomocí razítka jsem zamazal původní nápis na nabíječce.
** Písmena původního nápisu jsem rozřezal a transformoval na nápis VODPAD. Nejčastěji obdélníkovým označením + rotací/posunem

image::https://i.imgur.com/WKw6gpn.png[]

 6h práce
 
 Psaní dokumentace a uspořádávání repozitáře: 3.5h

== Progres

image::https://i.imgur.com/UP6mDlM.jpg[width=600]

image::https://i.imgur.com/XRPJ00S.jpg[width=600]

image::https://i.imgur.com/gu8lwHP.jpg[width=600]