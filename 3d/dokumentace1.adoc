= 3D

== Obsah
 * link:#chap-src[Zdroje]
 * link:#chap-i[Kapitola I - Kola]
 * link:#chap-ii[Kapitola II - Osy kol]
 * link:#chap-iii[Kapitola III - Kontejner]
 * link:#chap-iv[Kapitola IV - Solární panely]
 * link:#chap-v[Kapitola V - Světla]
 * link:#chap-vi[Kapitola VI - Vnitřek]
 * link:#chap-vii[Kapitola VII - A tak]
 * link:#chap-time[Souhrn času]
 * link:#chap-render[Finální rendery]

[#chap-src]
== Zdroje

=== Reference

* link:/3d/reference/me.jpg[Obrázek mě] (SRC: Fotila Rebeka Provazníková)
* link:/3d/reference/_DSC0022.jpg[Source Popelnice] (SRC: Fotil jsem)
* link:/3d/reference/_DSC0025.jpg[Source Popelnice (Cropped)]  (SRC: Fotil jsem)
* Celkem detailní blueprint kontejneru:

image::https://www.dopner.cz/user/documents/upload/technicke-parametry-kontejner1100_1.png[caption="celkem detailní blueprint kontejneru",width=1024]

=== Textury

* link:/3d/Materials/schadowplatz_4k.hdr[Environment Map] (CC0)
* link:/3d/Materials/moonless_golf_4k.hdr[Environment Map 2] (CC0)
* link:/3d/Materials/tvscreen.psd[Televizní obrazovka] (SRC: screenshot Netflixu)

==== Hliník (src: substance designer) (Color/Metallic/Normal/Roughness)

image:/3d/Materials/aluminum_basecolor.png[width=256]
image:/3d/Materials/aluminum_metallic.png[width=256]
image:/3d/Materials/aluminum_normal.png[width=256]
image:/3d/Materials/aluminum_roughness.png[width=256]

==== Neurčitý kov (src: substance designer) (Color/Metallic/Normal/Roughness)

image:/3d/Materials/wheeljoint_basecolor.png[width=256]
image:/3d/Materials/wheeljoint_metallic.png[width=256]
image:/3d/Materials/wheeljoint_normal.png[width=256]
image:/3d/Materials/wheeljoint_roughness.png[width=256]

==== Střed kol (src: substance designer) (Color/Metallic/Normal/Roughness)

image:/3d/Materials/wheel_metal_basecolor.png[width=256]
image:/3d/Materials/wheel_metal_metallic.png[width=256]
image:/3d/Materials/wheel_metal_normal.png[width=256]
image:/3d/Materials/wheel_metal_roughness.png[width=256]

==== Guma (src: substance designer) (Color/Metallic/Normal/Roughness)

image:/3d/Materials/wheel_rubber_basecolor.png[width=256]
image:/3d/Materials/wheel_rubber_metallic.png[width=256]
image:/3d/Materials/wheel_rubber_normal.png[width=256]
image:/3d/Materials/wheel_rubber_roughness.png[width=256]


[#chap-i]
== Kapitola I - Kola
* Na internetu jsem našel https://www.dopner.cz/kontejner-1100-l-plastovy-dopner--cerny[stránku prodávající městské kontejnery].
** Na ní jsem našel https://www.dopner.cz/user/documents/upload/technicke-parametry-kontejner1100_1.png[celkem detailní blueprint kontejneru].
*** Ten sice neobsahuje přesné rozměry kol, ovšem z obrázku jsem odtušil, že polovina rozdílu velikostí H a G bude přibližně rovna 1.5 šířky kolečka. Spočítal jsem tedy `((G-H)/2)*0.75` a vyšlo mi, že kola jsou široká cca `5,5cm`.
*** Obrázek sice obsahuje informaci, že "Kolo > 200 mm", ovšem není poznat, jestli se jedná o průměr či poloměr. https://www.mevatec.cz/Plastovy-kontejner-1100-l-cerny-d3070.htm[Na jiném eshopu] jsem naštěstí našel informaci, že kola mají *průměr* 200mm.
*** S těmito informacemi jsem tedy již mohl vytvořit v blenderu válec o správných rozměrech:

image::docimages/blender_2019-11-04_17-47-20.png[width=600]

* Pomocí Inset jsem podle fotek válec rozdělil na několik mezikruží.

image::docimages/blender_2019-11-04_17-59-49.png[width=600]

* Okraj kola jsem lehce zůžil a přitom využil Proportional Editing

image::docimages/blender_2019-11-04_18-05-08.png[width=600]

* Když jsem dělal vroubky (Měl jsem tento krok prohodit se zůžením okraje), každé mezikruží jsem nejdříve trochu vytáhnul pomocí Extrude, a poté jsem horní okraje posunul blíže k sobě, pomocí Scale a Move.
* Nakonec jsem je zaoblil pomocí Bevel.

image::docimages/blender_2019-11-04_18-27-10.png[width=600]

* Když jsem začínal s vnitřkem kola, z fotek jsem si všimnul, že má 5 děr a 5 plných částí.
* Naštěstí jsem na začátku vytvořil válec s rozlišením 80, a tak šlo kolo hezky rozdělit na 10 částí po 8-mi polygonech.
* Pomocí Dissolve Edges jsem tedy zredukoval hrany tak, aby mi zbylo 10 částí, z čehož jsem 5 vymazal tak, aby zůstala díra.

image::docimages/blender_2019-11-04_18-34-17.png[width=600]

* Objekt jsem nyní zesymetrizoval pomocí Mesh->Symmetrize.
* Vybíráním protilehlých okrajů a pomocí klávesové zkratky F (myslím, že spouští akci Create Face), jsem postupně zakryl všechny díry v objektu.
* U některých polygonů (nejčastěji, když jsem měl 3 spojené vertexy, kterým chyběla jedna hrana) jsem také použil CTRL+SELECT (Pick Shortest Path).

image::docimages/blender_2019-11-04_18-40-46.png[width=600]

* Nakonec jsem ještě označil, jaké okraje mají být měkké, a jaké tvrdé, a mohl jsem se vrhnout na UV Unwrap.
** Z UV Unwrappingu bohužel nemám žádný obrázek, ovšem postupoval jsem podle Blender Guru a všechny ostré okraje jsem označil jako Seam, dokud jsem nedostal mapu bez zkroucenin.

image::docimages/blender_2019-11-04_18-47-06.png[width=600]

* Nechtěl jsem mít kolo pouze dvoubarevné, a tak jsem model vyexportoval do FBX a naimportoval do programu Substance Designer.
* Snažil jsem se nějak replikovat špinavou gumu a plast. Není to perfektní, ale jako první pokus mi to stačí.

image::docimages/Substance_Designer_2019-11-04_19-45-04.png[width=600]

* Finální kolo po nahrání textur do Blenderu.

image::docimages/blender_2019-11-04_20-10-25.png[width=600]


[#chap-ii]
== Kapitola II - Osy/Držáky kol

Nejdříve jsem začal modelovat kovovou destičku, která spojuje kolo s popelnicí.

image::docimages/blender_2019-11-07_16-35-24.png[width=600]

Stačilo vytvořit 1 válec pro hlavní díru a 4 menší pro díry pro šrouby, a poté je odečíst pomocí modifieru Boolean od samotné destičky. Rohy destičky jsem zaoblil pomocí Bevel.

image::docimages/blender_2019-11-07_16-45-41.png[width=600]

Až teď jsem zjistil, že v polygonech nesmějí být díry, tudíž že polygony musejí být konvexní (nemít ostré úhly). Blender mi totiž vygeneroval tuto UV mapu:

image::docimages/blender_2019-11-07_21-10-45.png[width=600]

Naštěstí mi hned došlo v čem je problém, a tak jsem konkávní polygony rozkrájel tak, aby už konkávní nebyly. Nejvíce jsem při tom používal Edge Subdivide a Connect Vertices.

Pak jsem začal modelovat pant kola. Tady nešlo o nic víc, než o válec, který jsem v několika místech Extrudoval a rozšířil. 

image::docimages/blender_2019-11-07_21-47-32.png[width=600]

Teď jsem musel vymodelovat část, na kterou se připojuje samotné kolo. Nejdříve jsem pant protáhl směrem dolů, a pote pomocí Boolean operace připojil kvádr, jakožto kryt kola.

image::docimages/blender_2019-11-07_23-13-39.png[width=600]

Vytvořený objekt jsem polygonovým editováním ořezal a na jedné straně proporcionálním editováním zkosil. Přechod mezi válcem a přidaným kvádrem jsem zaoblil pomocí Bevel.

Narychlo jsem ještě přidal šroub. Jedná se pouze o válec o šesti hranách, který má zvětšenou hlavičku.

Nakonec jsem od vytvořeného objektu odečetl takovýto válec, aby vznikl otvor pro kolo:

image::docimages/blender_2019-11-07_23-13-22.png[width=600]

Tady je výsledek po přidání kola:

image::docimages/blender_2019-11-07_23-13-08.png[width=600]

image::docimages/blender_2019-11-07_23-14-29.png[width=600]

Texturu držáků jsem nakonec změnil z železa na hliník, ovšem to je vidět až ve finálním renderu (popř. ve scénách).


[#chap-iii]
== Kapitola III - Modelování kontejneru

Ze všecho nejdřív jsem si znova otevřel blueprint kontejneru, a podle rozměrů přidal do scény kvádr. Ten jsem na vrchu trochu rozšířil a ve spodní částí dovnitř vytáhl krajní třetiny spodku popelnice (vše se vyjasní za 3 obrázky) tak, abych vytvořil místo pro kola jako v nákresu.

Poté jsem začal modelovat místa na přišroubování kol.

image::docimages/blender_2019-11-09_17-09-18.png[width=600]

Hezky (řekl bych až mistrovsky) jsem je zaoblil pomocí bevel.

image::docimages/blender_2019-11-09_17-12-49.png[width=600]

__Tady se to vyjasní__. Na zkoušku na kontejner jsem "přišrouboval" jedno kolo. Sedí.

image::docimages/blender_2019-11-09_18-02-31.png[width=600]

Teď jsem se rozhodl, že se pustím do nástavce na stranách popelnice. Upřímně netuším, k čemu doopravdy slouží (předpokládám, že jako úchyty pro popelářská auta), ale rozhodl jsem, že v budoucnosti na VODPADu zůstaly z důvodu zachování tradičního vzhledu. Pomocí základních tvarů jsem si načrtl základ na upravování:

image::docimages/blender_2019-11-09_18-23-35.png[width=600]

Přidal jsem ještě samotné držadlo, zaoblil některé hrany, a nakonec nastavil shading některých polygonů na smooth (na fotce ještě nejsou změněny všechny).

image::docimages/blender_2019-11-09_19-10-34.png[width=600]

Teď jsem se mohl vrhnout na oblouk tváře popelnice. Nejdříve jsem na vrchní okraj použil subdivide, čímž mi vznikla hrana uprostřed, která bude sloužit jako vrchol oblouku. Tu jsem posunul hodně hodně vysoko, a poté vytvořil oblouk pomocí Bevel.
Pomocí Edge Subdivide a Extrude jsem začal přidávat detaily.

image::docimages/blender_2019-11-19_15-19-25.png[width=600]

image::docimages/blender_2019-11-19_15-33-12.png[width=600]

image::docimages/blender_2019-11-19_15-32-51.png[width=600]

Nakonec jsem celý kontejner zesymetrizoval pomocí Mesh->Symmetrize. 

[#chap-iv]
== Kapitola IV - Solární Panely

Až v tuto chvíli jsem si uvědomil, že modeluji dopravní prostředek z budoucnosti. Místo klasického poklopu jsem se tedy rozhodl udělat pohyblivou solární střechu.

Nejdříve jsem si tedy změřil, jak velký by měl být jeden panel. S naměřenou velikostí jsem vytvořil kvádr jako základ solárního panelu.

image::docimages/blender_2019-11-26_17-06-09.png[width=600]

Vrchní tvář panelu jsem pomocí Inset zmenšil, a poté pomocí Edge Subdivide rozdělil na 2x11 stejných polygonů. Poté jsem označil všechny vertexy této menší tváře a použil na ně Vertex Only Bevel. Tím se vytvořily pěkné hvězdicové tvary a hlavně samotné solární destičky/buňky. Všechny buňky jsem o pár mm vytáhl nahoru a všechny "hvězdy" dolů pomocí Extrude.

image::docimages/blender_2019-11-26_17-29-39.png[width=600]

Na všechny buňky jsem použil inset, a pak je všechny kromě jedné vymazal. Na tu zbylou jsem několikrát použil Edge Subdivide tak, aby na ní vznikly pruhy. Těm jsem změnil materiál, aby byly vidět.

image::docimages/blender_2019-11-26_17-51-58.png[width=600]

Nakonec jsem buňku zduplikoval pomocí SHIFT+D a s pomocí magnetu jsem vyplnil všechna vymazaná místa v jedné řadě. Pak jsem mesh zesymetrizoval, a zmergeoval blízké vertexy, aby v objektu nebyly žádné díry.

image::docimages/blender_2019-11-26_17-54-00.png[width=600]

Místo kloubu jsem vytvořil krátkou zahnutou kolej.

image::docimages/blender_2019-11-27_12-06-38.png[width=600]

image::docimages/blender_2019-11-27_13-08-52.png[]

Pomocí Duplicate (Link), Snap to Face a Align Rotation to Target jsem přidal celou střechu. Poté jsem ji ještě musel zarovnat pomocí snap to vertex (už bez rotace).

image::docimages/blender_2019-11-27_13-43-11.png[width=600]

image::docimages/blender_2019-11-27_13-45-21.png[width=600]

Přidal jsem Armature a příslušné kosti, abych mohl střechu nariggovat.

image::docimages/blender_2019-11-27_15-23-21.png[width=600]

Automatické nastavení vah dělalo velmi zvláštní věci, takže jsem musel manuálně nastavit každému panelu tu správnou kost a váhu.

image::docimages/blender_2019-11-27_15-22-35.png[width=600]

image::docimages/blender_2019-11-27_15-22-46.png[]

Pak jsem přidal každé kosti Rotation Constraint, aby se střecha chovala přirozeněji.
A proč mít nemít interaktivní střechu? S pomocí link:https://www.youtube.com/watch?v=K1Wj6DQoIRQ[návodu] jsem na vytvořenou kostru aplikoval IK (Inverse Kinematics), abych mohl celou střechou pohybovat pomocí pomocného pomocníka (pomocné kosti).

image::docimages/blender_2019-11-27_16-33-52.png[]

image::docimages/blender_2019-11-27_16-34-05.png[]

Hotovo

image::docimages/roofdone.png[width=1024]


[#chap-v]
== Kapitola V - Světla

Zde stačilo pomocí Edge Subdivide získat správný tvar na vnějšku VODPADu, a poté tvar vytáhnout a změnit materiály. 

image::docimages/blender_2019-11-28_17-33-07.png[width=600]

image::docimages/blender_2019-11-28_17-35-20.png[width=600]


[#chap-vi]
== Kapitola VI - Vnitřek VODPADu

Přemýšlel jsem, jak udělat vnitřek co nejpohodlnější. Napadlo mě celý vnitřek vypolstrovat. Naštěstí jsme na cvičení dělali simulaci látky, takže mi bylo jasné, že to půjde využít.

V jednom rohu jsem vytvořil místo na odkládání bot (nechtěli bychom VODPAD ušpinit). Zbytek podlahy jsem vyextrudoval a poté oddělil od zbytku objektu, abych mohl vyrobit polštář.

image::docimages/blender_2019-11-29_12-24-36.png[width=600]

Na jedné z kratších stěn jsem vytvořil dotykovou televizi s rozlišením 10K, s možností připojení přes WiFi, Bluetooth, i 6G síť. Kolem ní jsem vytvořil další základy polstrování.

image::docimages/blender_2019-11-29_18-14-42.png[width=600]

U každého z polštářů následoval následující postup. 

Nejdříve polštář zaoblím pomocí Subdivide Mesh. Pokud je to potřeba, přidám do původní meshe nějaké hrany, aby se zachoval tvar.

image::docimages/blender_2019-11-29_19-16-41.png[width=600]

image::docimages/blender_2019-11-29_18-33-51.png[]

Přidám polštáři simulaci látky.

image::docimages/blender_2019-11-29_18-34-12.png[]

Poté přidám okraj polštáře do zvláštní Vertex Group, a tu nastavím jako Pin Group v simulaci látky.

image::docimages/blender_2019-11-29_18-35-00.png[]

Vypnu pro tento objekt gravitaci a nakonec vytvořím nový objekt Force ve tvaru Point, který umístím pod/za polštář. Zapnu simulaci, a polštář se mi hezky zavlní a napne. Výsledek aplikuji. 

image::docimages/blender_2019-11-29_18-40-48.png[]

Hotovo:

image::docimages/Inside001.png[width=1024]


[#chap-vii]
== Kapitola VII - A TAK

Při kompozici renderu Detail1 jsem využil Texture Paint, abych na VODPAD přidal škrábance.

image::docimages/blender_2019-12-02_20-19-14.png[width=600]

Protože jsem potřeboval zachovat průhlednost, musel jsem vypnout Depth of Field. Rozmazání jsem poté nasimuloval ve Photoshopu pomocí filtru Tilt-shift 

image::docimages/Photoshop_2019-12-02_20-16-28.png[width=600]

Zde je ještě pár obrázků z vývoje:

image::docimages/Outside001.png[width=600]

image::docimages/Outside002.png[width=600]


[#chap-time]
== Čas

 Zatial: 2:56 + 1:39 + 2:31 (kola) + 2:52 + 1 + 1.25 (kontejner) + 2 + 5 (solární střecha) + 3:30 (dveře + paintjob) + 4 (vnitřek) + 6 (bugfixing a nastavování všech renderů) + pár hodin renderingu.

 Celkem tedy něco kolem 40 hodin.

[#chap-render]
== Finální Rendery

image::Render1-FullHD.png[width=1200]
image::Detail1.jpg[width=1200]
image::Render3.png[width=1200]

Navíc, za neuvěřitelných 9,90CZK:

image::Render4.png[width=1200]