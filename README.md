# Obsah

Tento repositář obsahuje moje práce na BI-MGA.

# [Bitmapa](dokumentace1.adoc)

* [Dokumentace](dokumentace1.adoc) je v rootu repositáře a zdrojové soubory bitmapy jsou ve složce [bitmap](bitmap/).
* Finální verze jsou ve složce bitmap pod názvy Uloha1Obr1.jpg a Uloha1Obr2.jpg

![](bitmap/Uloha1Obr1.jpg)

![](bitmap/Uloha1Obr2.jpg)

# [Vektory](dokumentace2.adoc)

* [Dokumentace](dokumentace2.adoc) je v rootu repositáře a zdrojové soubory vektory jsou ve složce [vector](vector/).
* Finální verze jsou ve složce vector pod názvy Uloha2Obr1.svg, Uloha2Obr2.svg a Uloha2Obr3.svg

![](vector/Uloha2Obr3@2x.png)

# [3D Objekt](dokumentace3.adoc)

* [Dokumentace](dokumentace3.adoc) je v rootu repositáře a zdrojové soubory bitmapy jsou ve složce [3d](3d/).
* Finální verze jsou ve složce 3d pod názvy Detail1.jpg, Render1-4K.png (a Render1-FullHD.png), a Render3.png

![](3d/Detail1.jpg)

![](3d/Render1-FullHD.png)

![](3d/Render3.png)
