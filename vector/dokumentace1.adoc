= Vektor 1 (Automatická vektorizace)

== Zdrojové soubory

* link:vector/Uloha2Obr1.ai[Source AI]
* link:vector/src/_DSC0020.NEF[Source Popelnice] (SRC: Fotil jsem)
* link:vector/src/_DSC0020(3).jpg[Source Popelnice (Cropped)]  (SRC: Fotil jsem)

== Postup

* Protože je popelnice velmi tmavá, musel jsem původní obrázek nejdříve upravit. V programu Fotky ve Win10 jsem fotku oříznul a zvýšil kontrast.

image::https://i.imgur.com/7xNRVHe.png[width=600]

image::vector/src/_DSC0020(3).jpg[]

* Upravený obrázek jsem importoval do ilustrátoru a zapnul automatickou vektorizaci.

image::https://i.imgur.com/Hblt2jc.png[]

* Po vektorizaci jsem se rozhodl zredukovat a přebarvit barvy. Illustrator má naštěstí jednu velmi mocnou (i když velice nešikovnou) pomůcku:

image::https://i.imgur.com/1rvWJZH.png[]

* Ta mi dovoluje jednak automaticky shloučit barvy do jedné, s tím, že si můžu vybrat výsledný počet barev, a jednak manuálně vybrat, které barvy se mají převést, a na jakou barvu.
** Bohužel, AI nedovoluje najít barvu tak, že by na ni člověk kliknul na obrázku. Je potřeba proklikat všechny odstíny (v mém případě jich bylo asi 300) a po jednom je změnit, aby člověk zjistil jakou část obrázku mění.

image::https://i.imgur.com/ixtsy7N.png[width=600]

** Zde je ještě UI po převedení na 5 barev.

image::https://i.imgur.com/YkGRyth.png[width=600]



 Zatial: 1.5h

== Progres

image::https://i.imgur.com/U0ySIdT.png[]

image::https://i.imgur.com/OsTN8Uz.png[]

link:vector/Uloha2Obr1.svg[Finální verze (Vypadá to, že asciidoc neumí embedovat svg)]


= Vektor 2 (Obrys/Omalovánka)

== Zdrojové soubory

* link:vector/Uloha2Obr2.ai[Source AI]
* link:vector/src/_DSC0022.NEF[Source Popelnice] (SRC: Fotil jsem)
* link:vector/src/_DSC0022.jpg[Source Popelnice (Cropped)] (SRC: Fotil jsem)

== Postup

* Zdrojovou fotku oříznu, aby měla menší velikost a rychleji se s ní pracovalo.
* Vytvořím novou vrstvu, do které budu obkreslovat.

image::https://i.imgur.com/9yzFat8.png[]

* Popelnici nejdříve nahrubo obráhnu pomocí Pen Tool.

image::https://i.imgur.com/X6YYSue.png[width=600]

* Pak jsem pomocí Nástroje pro přímý výběr upravil jednotlivé linky a rohy tak, aby více seděly.

image::https://i.imgur.com/xQvEgIt.png[width=600]

* Nakonec jsem upravil nastavcení tahu tak, aby měl měkké rohy a renderoval se "zvenku".

image::https://i.imgur.com/rHcsWdg.png[]

* Začal jsem obtahovat detaily

image::https://i.imgur.com/m8Wdw3j.png[width=600]

* Dodělal jsem kola a všem objektům jsem nastavil barvu čáry na černou a výplně na průhlednou. 

image::https://i.imgur.com/srGn50q.png[width=600]

* Začal jsem přidávat různé okrasné technické prvky. Zde jsem například použil ořezovou masku:

image::https://i.imgur.com/YijLtTe.png[width=600]

 4h 2min práce

== Progres

image::https://i.imgur.com/xQvEgIt.png[width=600]

image::https://i.imgur.com/uDIiCQo.png[width=600]

image::https://i.imgur.com/m8Wdw3j.png[width=600]

image::https://i.imgur.com/srGn50q.png[width=600]

link:vector/Uloha2Obr2.svg[Finální verze]

= Vektor 3 (Přeliv)

== Zdrojové soubory

* link:vector/Uloha2Obr2.ai[Source AI]
* link:vector/src/_DSC0022.NEF[Source Popelnice] (SRC: Fotil jsem)
* link:vector/src/_DSC0022.jpg[Source Popelnice (Cropped)] (SRC: Fotil jsem)

== Postup

* Zdrojovou fotku oříznu, aby měla menší velikost a rychleji se s ní pracovalo.
* Vytvořím novou vrstvu, do které budu obkreslovat.
* Začal jsem popelnici pokrývat obdélníky:

image::https://i.imgur.com/MjVp8Yr.png[width=600]

* První část jsem pokryl pomocí čtyřúhélníků a elips.

image::https://i.imgur.com/0JCwJXD.png[width=600]

* Zde jsem nejdříve spojil tyto dva kruhy pomocí Nástroje Prolnutí.

image::https://i.imgur.com/ZDda29m.png[width=600]

* Poté jsem tento tvar rozdělil pomocí Objekt->Prolnutí->Rozdělit.

image::https://i.imgur.com/RaypMVU.png[width=600]

* Poté jsem otevřel Cestář a všechny tvary spojil do jednoho.

image::https://i.imgur.com/YTMEB9d.png[width=600]

* Nakonec jsem tvar uhladil pomocí Objekt->Cesta->Zjednodušit.

image::https://i.imgur.com/wsstp0d.png[width=600]

* Ve zbytku obrázku jsem použil v podstatě jen kombinaci předchozích kroků.
** Vím, že se to zdá zjednodušené, ale skoro celou popelnici jsem pokryl tvořením čtyřúhleníků.
** Akorát na kola a čelo kontejneru jsem použil elipsu.
* Barvy na pozadí jsem vzal ze stránky color-hex v sekci Latest Palettes.

image::vector.jpg[width=600]

 3h 50min práce

== Progres

image::https://i.imgur.com/MjVp8Yr.png[width=600]

image::https://i.imgur.com/0JCwJXD.png[width=600]

image::https://i.imgur.com/9G6tTyy.png[width=600]

image::https://i.imgur.com/rCHHZOi.png[width=600]

image::vector.jpg[width=600]

link:vector/Uloha2Obr3.svg[Finální verze]